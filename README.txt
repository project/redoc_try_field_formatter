CONTENTS OF THIS FILE
---------------------
   
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * MAINTAINERS

INTRODUCTION
------------

This module allows to add new field formatter in drupal for redoc try 
functionality.

REQUIREMENTS
------------

This module requires js from "https://github.com/wll8/redoc-try".
Inside libraries folder add redoc-try library.
(libraris/redoc-redoc-try/dist/try.js)

INSTALLATION
------------

Its recommend to download this module with composer https://getcomposer.org/

CONFIGURATION
-------------

 1. Enable the module under /admin/modules.

 2. Add a new field formatter for link or file field in manage display.
    
MAINTAINERS
-----------

Current maintainers:
- Ramesh - https://www.drupal.org/u/drupalramesh

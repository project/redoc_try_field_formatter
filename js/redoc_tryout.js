(function ($, Drupal,drupalSettings) {
  'use strict';

  Drupal.behaviors.RedocBehavior = {
  attach: function (context, settings) {

    // Using once() 
    $('.redoc-link', context).once('remove-redoc').each(function () {
      var href = $('.redoc-link').attr('href');
	  initTry(href);
    });
  }
};

}(jQuery, Drupal,drupalSettings));
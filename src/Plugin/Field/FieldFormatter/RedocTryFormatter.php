<?php

namespace Drupal\redoc_try_field_formatter\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Plugin implementation of the 'redoc try formatter.
 *
 * @FieldFormatter(
 *   id = "redoc_with_try",
 *   label = @Translation("Redoc Try Out"),
 *   description = @Translation("Formats fields for redoc try"),
 *   field_types = {
 *     "file",
 *     "link"
 *   },
 * )
 */
class RedocTryFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];
    foreach ($items as $delta => $item) {
      // File field formatting.
      if ($item->entity) {
        $url = file_create_url($item->entity->getFileUri());
      }
      // Link field formatting.
      elseif ($this->fieldDefinition->getType() === 'link') {
        $url = $item->uri;
      }
      else {
        continue;
      }
      $element[$delta] = [
        '#theme' => 'redoc_try_field_item',
        '#field_name' => $this->fieldDefinition->getName(),
        '#delta' => $delta,
        '#url' => $url,
        '#attached' => [
          'library' => ['redoc_try_field_formatter/redoc_try_field_formatter.redoc',
            'redoc_try_field_formatter/redoc_try_field_formatter.redoc_try',
            'redoc_try_field_formatter/redoc_try_field_formatter.init',
          ],
        ],
      ];
    }
    return $element;
  }

}
